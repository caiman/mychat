# MyChat
Native Java Android chat app.

**WORK IN PROGRESS! See below what functionalities are currently (being)
implemented!**

Uses [Google Firebase](https://firebase.google.com) 
databases to store files, authentication and chat logs.
Different backend is planned for the future, see "Future plans" section.

### Currently working on:
First of all, learning basics of building Android apps.
Building a working chat app with features such as:
- [ ] user registration/login
    - [x] registration
        - [ ] proper registration screen allowing to fill user info
    - [x] login
        - [x] auto login on start
- [x] user profile
    - [x] user profile picture
    - [x] email address change
    - [x] password change
    - [x] auto load current user info
- [x] working message sending
    - [x] chat rooms
        - [x] creation
        - [x] sending messages to a chatroom
        - [x] name and picture
    - [ ] chat view
    - [x] chat list
- [ ] notifications

## Future plans
### Near future
* pings (no content high priority messages)
* sending attachments:
    * photos
    * videos
    * gifs
    * audio (voice messages, music)
* sending current localization
* notification settings
* stickers

Then work on polishing **visuals**, making it look good and 
consistent with Material Design specification, 
animations will probably also be added.
Other simple functionalities may also appear.

### Later plans
Features will be implemented mostly in that order.
Plans also involve using [Matrix](https://www.matrix.org) 
as a protocol instead of having own messaging
method basing on Google Firebase.
If changing messaging system would be successful, 
other Matrix basic functionalities would be implemented,
like VOIP, Video chat, Group Chat, OLM E2E encryption.
I have no idea how much work could it take, 
therefore I can't be sure about any of that.

I've also thought about payments, 
but I have no idea about that as well, backend has higher priority.

Other possibilities are XMPP or just another, non-Google backend system.