package pl.edu.pw.mini.student.saluchi.mychat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.Objects;

public class AuthenticateDialogFragment extends DialogFragment {
    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface AuthenticateDialogListener {
        void onDialogPositiveClick(DialogFragment dialog, String password);
        void onDialogNegativeClick(DialogFragment dialog);
    }
    // Use this instance of the interface to deliver action events
    AuthenticateDialogListener mListener;
    private LayoutInflater inflater;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = Objects.requireNonNull(inflater).inflate(R.layout.fragment_authenticate, null);
        final EditText mPasswordField = view.findViewById(R.id.mPasswordField);

        builder.setView(view)
                .setTitle(R.string.email_change_authentication)
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                    // Send the positive button event back to the host activity
                    mListener.onDialogPositiveClick(AuthenticateDialogFragment.this,
                            mPasswordField.getText().toString());
                })
                .setNegativeButton(android.R.string.cancel, (dialog, id) -> {
                    // Send the negative button event back to the host activity
                    mListener.onDialogNegativeClick(AuthenticateDialogFragment.this);
                });
        return builder.create();
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (AuthenticateDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement AuthenticateDialogListener");
        }
    }
}

