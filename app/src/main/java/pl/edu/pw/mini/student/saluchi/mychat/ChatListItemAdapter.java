package pl.edu.pw.mini.student.saluchi.mychat;

import android.content.Context;
import android.icu.util.TimeZone;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ChatListItemAdapter extends RecyclerView.Adapter<ChatListItemAdapter.MyViewHolder> {

    private static final String TAG = "ChatListItemAdapter";
    private ArrayList<Chatroom> mChatrooms;
    private Context mContext;

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    void setOnItemClickListener(OnItemClickListener listener) { this.listener = listener; }

    ChatListItemAdapter(Context context, ArrayList<Chatroom> chatrooms) {
        Log.d(TAG, "ChatListItemAdapter: ctor");
        this.mContext = context;
        this.mChatrooms = chatrooms;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_chatlistitem, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");

        if(mChatrooms.get(position).image != null)
            Glide.with(mContext).asBitmap().load(mChatrooms.get(position).image).into(holder.image);
        holder.name.setText(mChatrooms.get(position).name);
        holder.message.setText(mChatrooms.get(position).message);
        holder.timestamp.setText(mChatrooms.get(position)
                .getTime(TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT_GMT)));
    }

    @Override
    public int getItemCount() {
        return mChatrooms.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        TextView message;
        TextView timestamp;

        MyViewHolder(final View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.mChatListAvatar);
            name = itemView.findViewById(R.id.mChatListName);
            message = itemView.findViewById(R.id.mChatListMessage);
            timestamp = itemView.findViewById(R.id.mChatListTimestamp);
            // Setup the click listener
            itemView.setOnClickListener(v -> {
                // Triggers click upwards to the adapter on click
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(itemView, position);
                    }
                }
            });
        }
    }
}
