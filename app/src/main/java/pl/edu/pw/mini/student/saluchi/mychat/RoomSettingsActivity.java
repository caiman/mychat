package pl.edu.pw.mini.student.saluchi.mychat;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;


public class RoomSettingsActivity extends AppCompatActivity {
    private static final String TAG = RoomSettingsActivity.class.getName();
    public static final int PICK_IMAGE = 1;

    private Button mSaveButton;
    private Button mCancelButton;
    private Button mDeleteRoomButtom;
    private Button mLeaveRoomButton;
    private EditText mRoomNameField;
    private TextView mRoomIdTextView;
    private ImageView mRoomPicImageView;
    String roomId;

    private StorageReference storageRef;
    private Uri roomImageUri;
    private FirebaseFirestore db;
    private FirebaseStorage storage;
    private Map<String, Object> roomData;
    private DocumentReference docRef;
    private FirebaseAuth mAuth;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_settings);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.chatroom_details);

        // receive chat id
        Intent intent = getIntent();
        roomId = intent.getStringExtra("chatid");

        // initialize widgets
        mSaveButton = findViewById(R.id.mSaveButton);
        mCancelButton = findViewById(R.id.mCancelButton);
        mDeleteRoomButtom = findViewById(R.id.mDeleteRoomButton);
        mLeaveRoomButton = findViewById(R.id.mLeaveRoomButton);
        mRoomNameField = findViewById(R.id.mRoomNameEditText);
        mRoomIdTextView = findViewById(R.id.mRoomIdTextView);
        mRoomPicImageView = findViewById(R.id.mRoomPictureImageView);

        mRoomIdTextView.setText(roomId);

        // initialize firebase
        db = FirebaseFirestore.getInstance();
        docRef = db.collection("chatrooms")
                .document(roomId);

        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference()
                .child("chatrooms").child(roomId);

        mAuth = FirebaseAuth.getInstance();
        uid = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        docRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    roomData = document.getData();
                    Log.d(TAG, "DocumentSnapshot data: " + roomData);
                    // fill fields with info from the database
                    if(roomData.containsKey("name"))
                        mRoomNameField.setText(roomData.get("name").toString());
                    if(roomData.containsKey("avatar"))
                        loadAvatar(roomData.get("avatar").toString());
                } else {
                    Log.d(TAG, "No such document");
                }
            } else {
                Log.d(TAG, "get failed with ", task.getException());
            }
        });

        mSaveButton.setOnClickListener(v -> {
            if(roomData == null) return;
            // upload info to the database
            final String enteredName = mRoomNameField.getText().toString();
            if(!enteredName.equalsIgnoreCase((String)roomData.get("name")))
                roomData.put("name", mRoomNameField.getText().toString());
            if(roomImageUri != null)
                roomData.put("avatar", roomImageUri.toString());
            docRef.set(roomData);
            finish();
        });

        mRoomPicImageView.setOnClickListener(v -> {
            Intent pickIntent = new Intent(Intent.ACTION_PICK);
            pickIntent.setType("image/*");
            startActivityForResult(pickIntent, PICK_IMAGE);
        });

        mCancelButton.setOnClickListener(v -> finish());

        mLeaveRoomButton.setOnClickListener(v -> {
            docRef.get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        roomData = document.getData();
                        Log.d(TAG, "DocumentSnapshot data: " + roomData);
                        if(roomData.containsKey("participants")) {
                            ArrayList<String> participants = (ArrayList<String>) roomData.get("participants");
                            participants.removeIf((String element) -> element.equals(uid));
                            // if it was the last person, delete room
                            if (participants.size() == 0) docRef.delete();
                            // else update the document
                            else docRef.update("participants", participants)
                                    .addOnSuccessListener(aVoid -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                            .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                            Log.d(TAG, "onCreate: " + participants);
                        }

                    } else {
                        Log.d(TAG, "onCreate: Cannot leave room");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            });
            Log.d(TAG, "leaveRoom:success");
            Intent homeIntent =
                    new Intent(RoomSettingsActivity.this, HomeActivity.class);
            startActivity(homeIntent);
        });

        mDeleteRoomButtom.setOnClickListener(v -> {
            docRef.delete();
            Log.w(TAG, "deleteRoom:success");
            Intent homeIntent =
                    new Intent(RoomSettingsActivity.this, HomeActivity.class);
            startActivity(homeIntent);
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            final Uri imageUri = data.getData();
            Log.w(TAG, "image picked: " + imageUri);
            mSaveButton.setEnabled(false);
            mSaveButton.setText(R.string.uploading);

            final StorageReference picRef = storageRef.child(Objects.requireNonNull(imageUri).getLastPathSegment());
            final UploadTask uploadTask = picRef.putFile(imageUri);

            Task<Uri> urlTask = uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    throw Objects.requireNonNull(task.getException());
                }
                // Continue with the task to get the download URL
                return picRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    roomImageUri = task.getResult();
                    Log.w(TAG, "image uploaded: " + roomImageUri);
                    loadAvatar(roomImageUri.toString());
                    mSaveButton.setText(R.string.save);
                    mSaveButton.setEnabled(true);
                } else {
                    Log.w(TAG, "image upload failed" + task.getException());
                    Toast.makeText(RoomSettingsActivity.this, R.string.upload_failed, Toast.LENGTH_SHORT).show();
                    mSaveButton.setText(R.string.upload_failed);
                }
            });
        }
    }

    private void loadAvatar(String address) {
        Glide.with(RoomSettingsActivity.this)
                .load(address)
                .into(mRoomPicImageView);
    }

}
