package pl.edu.pw.mini.student.saluchi.mychat;

import android.support.annotation.NonNull;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Chatroom implements Comparable<Chatroom> {
    String name, image, message, id;
    Long timestamp;

    public Chatroom(String name, String image, String message, Long timestamp, String id) {
        this.name = name;
        this.image = image;
        this.message = message;
        this.timestamp = timestamp;
        this.id = id;
    }

    public String getTime(String timezone){
        // convert timestamp to hour
        final DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("HH:mm");
        final long unixTime = timestamp;
        // older APIS don't like the method below:
        return Instant.ofEpochSecond(unixTime)
                .atZone(ZoneId.of(timezone))
                .format(formatter);

    }

    @Override
    public int compareTo(@NonNull Chatroom o) {
        return (this.timestamp < o.timestamp ? -1 :
                (this.timestamp.equals(o.timestamp) ? 0 : 1));
    }
}
