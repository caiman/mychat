package pl.edu.pw.mini.student.saluchi.mychat;

import android.content.Context;
import android.icu.util.TimeZone;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private static final String TAG = "MessageAdapter";
    private ArrayList<Message> mMessages;
    private Context mContext;

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    MessageAdapter(Context context, ArrayList<Message> messages) {
        Log.d(TAG, "MessageAdapter: ctor");
        this.mContext = context;
        this.mMessages = messages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_message, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        if(holder.image != null)
            Glide.with(mContext).asBitmap().load(mMessages.get(position).avatar).into(holder.image);
        holder.message.setText(mMessages.get(position).contents);
        holder.sender.setText(mMessages.get(position).sender);
        holder.timestamp.setText(mMessages.get(position)
                .getTime(TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT_GMT)));
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView message;
        TextView sender;
        TextView timestamp;

        MyViewHolder(final View itemView) {
            super(itemView);
            sender = itemView.findViewById(R.id.mConversationSenderTextView);
            image = itemView.findViewById(R.id.mConversationAvatar);
            message = itemView.findViewById(R.id.mConversationMessage);
            timestamp = itemView.findViewById(R.id.mMessageTimestamp);

            // Setup the click listener
            itemView.setOnClickListener(v -> {
                // Triggers click upwards to the adapter on click
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(itemView, position);
                    }
                }
            });
        }
    }
}
