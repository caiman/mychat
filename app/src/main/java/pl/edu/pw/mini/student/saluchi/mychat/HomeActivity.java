package pl.edu.pw.mini.student.saluchi.mychat;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class HomeActivity extends AppCompatActivity implements AddRoomDialogFragment.AddRoomDialogListener {
    private static final String TAG = HomeActivity.class.getName();

    private FloatingActionButton fab;
    private RecyclerView mChatList;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseFirestore db;
    private String uid;

    private ArrayList<Chatroom> mChatRooms = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        fab = findViewById(R.id.addRoomFab);

        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        assert user != null;
        uid = user.getUid();

        fab.setOnClickListener(v -> new AddRoomDialogFragment().show(getFragmentManager(), "addroom"));

        //// download all rooms
        CollectionReference rooms = db.collection("chatrooms");

        rooms.whereArrayContains("participants", uid).addSnapshotListener((snapshots, e) -> {
            if (e != null) {
                Log.w(TAG, "listen:error", e);
                Toast.makeText(HomeActivity.this, "Cannot download chatrooms", Toast.LENGTH_SHORT).show();
                return;
            }

            for (DocumentChange dc : Objects.requireNonNull(snapshots).getDocumentChanges()) {
                switch (dc.getType()) {
                    case ADDED:
                        final Map<String, Object> chatRoomMap = dc.getDocument().getData();
                        Log.d(TAG, "onCreate: New conversation: " + chatRoomMap);

                        String roomId = dc.getDocument().getId();
                        String roomImage = null,
                                roomName = roomId,
                                roomMessage = "No messages";
                        //TODO: ^
                        Long roomTimestamp = 0L;

                        if(Objects.requireNonNull(chatRoomMap).containsKey("avatar"))
                            roomImage = chatRoomMap.get("avatar").toString();
                        if(Objects.requireNonNull(chatRoomMap).containsKey("name"))
                            roomName = chatRoomMap.get("name").toString();
                        if(Objects.requireNonNull(chatRoomMap).containsKey("message"))
                            roomMessage = chatRoomMap.get("message").toString();
                        if(Objects.requireNonNull(chatRoomMap).containsKey("timestamp"))
                            roomTimestamp = Long.parseLong(chatRoomMap.get("timestamp").toString());
                        Chatroom newChatroom = new Chatroom(roomName, roomImage,
                                roomMessage, roomTimestamp, roomId);
                        mChatRooms.add(newChatroom);
                        initRecyclerView();
                        break;

                    case MODIFIED:
                        String modifiedId = dc.getDocument().getId();
                        final Map<String, Object> modifiedMap = dc.getDocument().getData();
                        Log.d(TAG, "Modified chatroom: " + modifiedId + modifiedMap);

                        roomId = dc.getDocument().getId();
                        roomImage = null;
                        roomName = roomId;
                        roomMessage = "No messages";
                        //TODO: ^
                        roomTimestamp = 0L;

                        if(Objects.requireNonNull(modifiedMap).containsKey("avatar"))
                            roomImage = modifiedMap.get("avatar").toString();
                        if(Objects.requireNonNull(modifiedMap).containsKey("name"))
                            roomName = modifiedMap.get("name").toString();
                        if(Objects.requireNonNull(modifiedMap).containsKey("message"))
                            roomMessage = modifiedMap.get("message").toString();
                        if(Objects.requireNonNull(modifiedMap).containsKey("timestamp"))
                            roomTimestamp = Long.parseLong(modifiedMap.get("timestamp").toString());

                        for (Chatroom chatroom : mChatRooms) {
                            if (chatroom.id.equals(modifiedId)) {
                                chatroom.timestamp = roomTimestamp;
                                chatroom.image = roomImage;
                                chatroom.name = roomName;
                                chatroom.message = roomMessage;
                                break;
                            }
                        }


                        initRecyclerView();
                        break;
                    case REMOVED:
                        String removedChatRoomId = dc.getDocument().getId();
                        Log.d(TAG, "Removed chatroom: " + removedChatRoomId);

                        for (Chatroom chatroom : mChatRooms) {
                            if (chatroom.id.equals(removedChatRoomId)) {
                                mChatRooms.remove(chatroom);
                                break;
                            }
                        }
                        initRecyclerView();
                        break;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        //noinspection SwitchStatementWithTooFewBranches
        switch (item.getItemId()) {
            case R.id.account:
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String recipientUid;

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String input, boolean existing) {
        if (dialog.getTag().equals("addroom")) {
            if (existing) {
                DocumentReference docRef = db.collection("chatrooms").document(input);
                docRef.get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Map<String, Object> roomData = document.getData();
                            Log.d(TAG, "DocumentSnapshot data: " + roomData);
                            if(roomData.containsKey("participants")) {
                                ArrayList<String> participants = (ArrayList<String>) roomData.get("participants");
                                // check if aren't already in the room
                                if(participants.contains(uid)) {
                                    Toast.makeText(this, "You are already a member of this room",
                                            Toast.LENGTH_SHORT).show();
                                    // start the activity
                                    Intent intent = new Intent(HomeActivity.this, ConversationActivity.class);
                                    intent.putExtra("chatid", input);
                                    startActivity(intent);
                                    return;
                                }
                                // add the user to the room
                                participants.add(uid);
                                // update the document
                                docRef.update("participants", participants)
                                        .addOnSuccessListener(aVoid -> {
                                            Log.d(TAG, "DocumentSnapshot successfully updated!");
                                            Intent intent = new Intent(HomeActivity.this, ConversationActivity.class);
                                            intent.putExtra("chatid", input);
                                            startActivity(intent);
                                        })
                                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                                Log.d(TAG, "onCreate: " + participants);
                            }
                        } else {
                            Toast.makeText(this, "Room " + input + " does not exist",
                                    Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Room " + input + " does not exist");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                });
            }
            else {
                Log.w(TAG, "new room with user " + input);

                // get recipient uid
                db.collection("users")
                        .whereEqualTo("email", input)
                        .get()
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                    recipientUid = document.getId();
                                    Log.d(TAG, recipientUid + " => " + document.getData());
                                    // create and enter a new room
                                    DocumentReference chatroomRef =
                                            db.collection("chatrooms").document();
                                    ArrayList<String> participants = new ArrayList<>();
                                    participants.add(uid);
                                    participants.add(recipientUid);
                                    //TODO: more recipients
                                    Map<String, Object> roomMap = new HashMap<>();
                                    roomMap.put("timestamp",
                                            String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
                                    roomMap.put("participants", participants);

                                    chatroomRef.set(roomMap);
                                    Log.d(TAG, "chatroomRef" + " => " + chatroomRef.getPath());
                                    Log.d(TAG, "onDialogPositiveClick: room created: " + roomMap);

                                    // start the activity
                                    Intent intent = new Intent(HomeActivity.this, ConversationActivity.class);
                                    intent.putExtra("chatid", chatroomRef.getId());
                                    startActivity(intent);
                                    return;
                                }
                                Toast.makeText(HomeActivity.this, R.string.user_not_found, Toast.LENGTH_SHORT).show();

                            } else {
                                Log.d(TAG, "Error getting documents: ", task.getException());
                                Toast.makeText(HomeActivity.this, R.string.user_not_found, Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        }

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) { }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: called");
        mChatList = findViewById(R.id.mChatListRecyclerView);
        ChatListItemAdapter adapter = new ChatListItemAdapter(this, mChatRooms);
        adapter.setOnItemClickListener((itemView, position) -> {
            Intent intent = new Intent(HomeActivity.this, ConversationActivity.class);
            intent.putExtra("chatid", mChatRooms.get(position).id);
            startActivity(intent);

        });
        mChatList.setAdapter(adapter);
        mChatList.setLayoutManager(new LinearLayoutManager(this));
    }
}
