package pl.edu.pw.mini.student.saluchi.mychat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ConversationActivity extends AppCompatActivity {
    private static final String TAG = "ConversationActivity";

    private ActionBar actionBar;
    private RecyclerView mMessagesRecyclerView;
    private ImageButton sendButton;
    private EditText messageField;
    private ArrayList<Message> mMessages;
    private String chatId;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseFirestore db;
    private String uid;
    private DocumentReference chatRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        // initialize widgets
        actionBar = getSupportActionBar();
        sendButton = findViewById(R.id.mSendButton);
        messageField = findViewById(R.id.mMessageField);
        // initialize lists
        mMessages = new ArrayList<>();

        // receive chat id
        Intent intent = getIntent();
        chatId = intent.getStringExtra("chatid");
        actionBar.setTitle(chatId);

        // connect to the firestore
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);
        chatRef = db.collection("chatrooms")
                .document(chatId);

        // get uid
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        assert user != null;
        uid = user.getUid();

        // set sendButton listener
        sendButton.setOnClickListener(v -> {
            sendMessage(messageField.getText().toString());
            Log.d(TAG, "onClick: message sent: " + messageField.getText().toString());
            messageField.setText("");
        });

        // set realtime chat name update
        chatRef.addSnapshotListener(((documentSnapshot, e) -> {
            if (e != null) {
                Log.w(TAG, "listen:error", e);
                return;
            }
            if (documentSnapshot != null && documentSnapshot.exists()) {
                Map<String, Object> dataMap = documentSnapshot.getData();
                Log.d(TAG, "Current data: " + dataMap);
                if(dataMap.containsKey("name"))
                    actionBar.setTitle(dataMap.get("name").toString());
            } else {
                Log.d(TAG, "Current data: null");
            }

        }));


        // set realtime updates/download messages
        chatRef.collection("messages")
                .addSnapshotListener((snapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "listen:error", e);
                        return;
                    }

                    for (DocumentChange dc : Objects.requireNonNull(snapshots).getDocumentChanges()) {
                        switch (dc.getType()) {
                            case ADDED:
                                final Map<String, Object> messageMap = dc.getDocument().getData();
                                Log.d(TAG, "New message: " + messageMap);
                                DocumentReference senderRef = db.collection("users").
                                        document(messageMap.get("sender").toString());
                                senderRef.get().addOnCompleteListener(task -> {
                                    String avatar = null;
                                    String name = null;
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document = task.getResult();
                                        if (Objects.requireNonNull(document).exists()) {
                                            Map<String, Object> userData = document.getData();
                                            if(Objects.requireNonNull(userData).containsKey("avatar"))
                                                avatar = userData.get("avatar").toString();
                                            name = Objects.requireNonNull(userData).containsKey("name") ?
                                                    userData.get("name").toString() : userData.get("email").toString();
                                        } else {
                                            Log.d(TAG, "No such document");
                                        }
                                        Message message = new Message(name, messageMap.get("contents").toString(),
                                                Long.parseLong(messageMap.get("timestamp").toString()),
                                                avatar);
                                        mMessages.add(message);
                                        initRecyclerView();

                                    } else {
                                        Log.d(TAG, "get failed with ", task.getException());
                                        //TODO: ^
                                    }
                                });
                                break;

                            case MODIFIED:
                                Log.d(TAG, "Modified message: " + dc.getDocument().getData());
                                break;
                            case REMOVED:
                                Log.d(TAG, "Removed message: " + dc.getDocument().getData());
                                break;
                        }
                    }

                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.conversation_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        //noinspection SwitchStatementWithTooFewBranches
        switch (item.getItemId()) {
            case R.id.info:
                Intent intent = new Intent(ConversationActivity.this, RoomSettingsActivity.class);
                intent.putExtra("chatid", chatId);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: called");
        mMessagesRecyclerView = findViewById(R.id.mMessagesRecyclerView);
        MessageAdapter adapter = new MessageAdapter(this, mMessages);
        Collections.sort(mMessages);
        //adapter.setOnItemClickListener(new MessageAdapter.OnItemClickListener() {
        //    @Override
        //    public void onItemClick(View itemView, int position) {
        //        Toast.makeText(ConversationActivity.this, "Message clicked", Toast.LENGTH_SHORT).show();
        //        //TODO: maybe better on hold
        //    }
        //});
        mMessagesRecyclerView.setAdapter(adapter);
        mMessagesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMessagesRecyclerView.smoothScrollToPosition(mMessages.size()-1);
    }

    private void sendMessage(String contents){
        if(contents.isEmpty()) {
            Toast.makeText(this, "Error - Can't send an empty message.", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, Object> message = new HashMap<>();
        message.put("sender", uid);
        message.put("contents", contents);
        String timeStamp = String
                .valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        message.put("timestamp", timeStamp);
        chatRef.collection("messages").document().set(message);

        // update conversation timestamp
        Map<String, Object> data = new HashMap<>();
        data.put("timestamp", timeStamp);

        // update conversation last message
        data.put("message", contents);
        chatRef.set(data, SetOptions.merge());

        Log.d(TAG, "sendMessage: message sent.");
    }
}
