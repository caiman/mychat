package pl.edu.pw.mini.student.saluchi.mychat;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Map;
import java.util.Objects;


public class ProfileActivity extends AppCompatActivity
        implements AuthenticateDialogFragment.AuthenticateDialogListener, PasswordChangeDialogFragment.PasswordChangeDialogListener {
    private static final String TAG = ProfileActivity.class.getName();
    public static final int PICK_IMAGE = 1;

    private Button mSaveButton;
    private Button mCancelButton;
    private Button mSignOutButton;
    private Button mChangePasswordButton;
    private EditText mEmailField;
    private EditText mNameField;

    private FirebaseAuth mAuth;
    private StorageReference storageRef;
    private FirebaseUser user;
    private Uri profileImageUri;
    private ImageView mProfilePictureImage;
    private FirebaseFirestore db;
    private FirebaseStorage storage;
    private Map<String, Object> userData;
    private DocumentReference docRef;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.account);

        // initialize widgets
        mSaveButton = findViewById(R.id.mSaveButton);
        mCancelButton = findViewById(R.id.mCancelButton);
        mSignOutButton = findViewById(R.id.mSignOutButton);
        mChangePasswordButton = findViewById(R.id.mChangePasswordButton);
        mEmailField = findViewById(R.id.mEmailEditText);
        mNameField = findViewById(R.id.mNameEditText);
        mProfilePictureImage = findViewById(R.id.mRoomPictureImageView);

        // initialize firebase
        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        uid = Objects.requireNonNull(user).getUid();
        storageRef = storage.getReference()
                .child("avatars").child(uid);
        docRef = db.collection("users")
                .document(uid);


        docRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    userData = document.getData();
                    Log.d(TAG, "DocumentSnapshot data: " + userData);

                    // fill fields with info from the database
                    if(userData.containsKey("email"))
                        mEmailField.setText(userData.get("email").toString());
                    else {
                        // put email address to the database
                        mEmailField.setText(user.getEmail());
                        userData.put("email", user.getEmail());
                        docRef.set(userData);
                    }
                    if(userData.containsKey("name"))
                        mNameField.setText(userData.get("name").toString());
                    if(userData.containsKey("avatar"))
                        loadAvatar(userData.get("avatar").toString());
                } else {
                    Log.d(TAG, "No such document");
                }
            } else {
                Log.d(TAG, "get failed with ", task.getException());
            }
        });

        mSaveButton.setOnClickListener(v -> {
            if(userData == null) return;
            // upload info to the database
            final String enteredEmail = mEmailField.getText().toString();
            final String enteredName = mNameField.getText().toString();
            if(!enteredName.equalsIgnoreCase((String)userData.get("name")))
                userData.put("name", mNameField.getText().toString());
            if(profileImageUri != null)
                userData.put("avatar", profileImageUri.toString());
            docRef.set(userData);
            if(!enteredEmail.equalsIgnoreCase((String)userData.get("email"))) {
                AuthenticateDialogFragment authenticateDialogFragment = new AuthenticateDialogFragment();
                authenticateDialogFragment.show(getFragmentManager(), "emailpassword");
            }
            else finish();
        });

        mProfilePictureImage.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_IMAGE);
        });

        mCancelButton.setOnClickListener(v -> finish());

        mSignOutButton.setOnClickListener(v -> {
            mAuth.signOut();
            Log.w(TAG, "signOut:success");
            Intent intent =
                    new Intent(ProfileActivity.this, LoginActivity.class);
            // remove all previous activities
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });

        mChangePasswordButton.setOnClickListener(v -> new PasswordChangeDialogFragment().show(getFragmentManager(), "passwordchange"));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            final Uri imageUri = data.getData();
            Log.w(TAG, "image picked: " + imageUri);
            mSaveButton.setEnabled(false);
            mSaveButton.setText(R.string.uploading);

            final StorageReference picRef = storageRef.child(Objects.requireNonNull(imageUri).getLastPathSegment());
            final UploadTask uploadTask = picRef.putFile(imageUri);

            Task<Uri> urlTask = uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    throw Objects.requireNonNull(task.getException());
                }
                // Continue with the task to get the download URL
                return picRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    profileImageUri = task.getResult();
                    Log.w(TAG, "image uploaded: " + profileImageUri);
                    loadAvatar(profileImageUri.toString());
                    mSaveButton.setText(R.string.save);
                    mSaveButton.setEnabled(true);
                } else {
                    Log.w(TAG, "image upload failed" + task.getException());
                    Toast.makeText(ProfileActivity.this, R.string.upload_failed, Toast.LENGTH_SHORT).show();
                    mSaveButton.setText(R.string.upload_failed);
                }
            });
        }
    }

    private void loadAvatar(String address) {
        Glide.with(ProfileActivity.this)
                .load(address)
                .into(mProfilePictureImage);
    }

    private void changeEmail(final String newEmail, final String password, final FirebaseUser user){
        AuthCredential credential = EmailAuthProvider
                .getCredential(Objects.requireNonNull(user.getEmail()), password);
        user.reauthenticate(credential)
                .addOnCompleteListener(task -> {
                    Log.d(TAG, "User re-authenticated.");
                    user.updateEmail(newEmail)
                            .addOnCompleteListener(task1 -> {
                                if (task1.isSuccessful()) {
                                    Log.d(TAG, "User email address updated.");
                                    userData.put("email", newEmail);
                                    docRef.set(userData);
                                    Log.d(TAG, "email updated to " + newEmail);
                                    Toast.makeText(ProfileActivity.this, R.string.email_updated, Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Log.d(TAG, "User email address update error.");
                                    Toast.makeText(ProfileActivity.this, R.string.email_update_error, Toast.LENGTH_SHORT).show();
                                }
                            });
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "User authentication error.");
                    Toast.makeText(ProfileActivity.this, R.string.authentication_error, Toast.LENGTH_SHORT).show();
                });
    }

    private void changePassword(final FirebaseUser user, final String newPassword, final String currentPassword) {
        AuthCredential credential = EmailAuthProvider
                .getCredential(Objects.requireNonNull(user.getEmail()), currentPassword);
        user.reauthenticate(credential)
                .addOnCompleteListener(task -> {
                    Log.d(TAG, "User re-authenticated.");
                    if(currentPassword.equals(newPassword)) {
                        Log.d(TAG, "The new and the current password are the same");
                        Toast.makeText(this, R.string.passwords_the_same, Toast.LENGTH_SHORT).show();
                        return;

                    }
                    user.updatePassword(newPassword)
                            .addOnCompleteListener(task1 -> {
                                if (task1.isSuccessful()) {
                                    Log.d(TAG, "User password updated.");
                                    Toast.makeText(this, R.string.password_updated, Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Log.d(TAG, "User password NOT updated.");
                                    Toast.makeText(this, R.string.password_not_updated, Toast.LENGTH_SHORT).show();

                                }
                            });
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "User authentication error.");
                    Toast.makeText(ProfileActivity.this, R.string.authentication_error, Toast.LENGTH_SHORT).show();
                });
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String password) {
        if(dialog.getTag().equals("emailpassword")) {
            Log.d(TAG, dialog.getTag() + "positiveclick");
            String enteredEmail = mEmailField.getText().toString();
            changeEmail(enteredEmail, password, user);
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String newpassword, String currentpassword, boolean match) {
        if(dialog.getTag().equals("passwordchange")) {
            Log.d(TAG, dialog.getTag() + "positiveclick");
            if(!match) {
                Toast.makeText(ProfileActivity.this, R.string.passwords_match, Toast.LENGTH_SHORT).show();
                return;
            }
            changePassword(user, newpassword, currentpassword);
        }
    }


    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Log.d(TAG, "negativeclick");

    }
}
