package pl.edu.pw.mini.student.saluchi.mychat;

import android.support.annotation.NonNull;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Message implements Comparable<Message> {
    String sender;
    String contents;
    String avatar;
    Long timestamp;

    public Message(String sender, String contents, Long timestamp, String avatar) {
        this.sender = sender;
        this.contents = contents;
        this.timestamp = timestamp;
        this.avatar = avatar;
    }

    public String getTime(String timezone){
        // convert timestamp to hour
        final DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("HH:mm");
        final long unixTime = timestamp;
        // older APIS don't like the method below:
        return Instant.ofEpochSecond(unixTime)
                .atZone(ZoneId.of(timezone))
                .format(formatter);

    }

    @Override
    public int compareTo(@NonNull Message o) {
        return (this.timestamp < o.timestamp ? -1 :
                (this.timestamp == o.timestamp ? 0 : 1));
    }
}
