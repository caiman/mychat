package pl.edu.pw.mini.student.saluchi.mychat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getName();

    private Button mLoginButton, mRegisterButton;
    private EditText mEmailField, mPasswordField;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginButton = findViewById(R.id.mLoginButton);
        mRegisterButton = findViewById(R.id.mRegisterButton);
        mEmailField = findViewById(R.id.mEmailField);
        mPasswordField = findViewById(R.id.mPasswordField);

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            //TODO: react to invalid account
            Log.d(TAG, "autoSignIn:success");
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
        }

        // REGISTRATION
        mRegisterButton.setOnClickListener(v -> {
            //TODO: enter name and set an avatar
            // maybe use a modified ProfileActivity?

            final String email = mEmailField.getText().toString();
            final String password = mPasswordField.getText().toString();

            if( email.isEmpty() || password.isEmpty() ) {
                Toast.makeText(LoginActivity.this, R.string.fill_fields, Toast.LENGTH_SHORT).show();
                return;
            }

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(LoginActivity.this, task -> {
                        if (task.isSuccessful()) {
                            Log.w(TAG, "registerWithEmail:success", task.getException());
                            // add info to database
                            FirebaseFirestore db = FirebaseFirestore.getInstance();
                            Map<String, Object> user1 = new HashMap<>();
                            user1.put("email", email);
                            // Add a new document with a generated ID
                            db.collection("users")
                                    .document(Objects.requireNonNull(mAuth.getUid()))
                                    .set(user1);
                            Toast.makeText(LoginActivity.this, "Registered successfully",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.w(TAG, "registerWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Please enter correct data",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });


        });

        // LOGIN
        mLoginButton.setOnClickListener(v -> {

            final String email = mEmailField.getText().toString();
            final String password = mPasswordField.getText().toString();

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(LoginActivity.this, task -> {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });


        });

    }
}
